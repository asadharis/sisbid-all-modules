# SISBID 2016: All Modules

Welcome to the SISBID 2016 course repository. Each of the five modules have been organized and grouped within this repository. 

## Information for Downloading Course Material
To obtain all the course material, we have multiple options.

1. Use the 'Clone or download' button on the Github webpage for the repo.
2. In terminal/command line run the following command to clone the repo
```shell
git clone https://github.com/SISBID/SISBID-All-Modules.git
```

## Information for Instructors 
Instructors are recommended to clone the repository instead of downloading all files as a .zip file. This will import all tags and repo history.

To add tags (release version in Github) we require the following: 
```shell
# Create a tag for a specific commit
# We do not require the full SHA here
git tag [tagname] [commit SHA]

# Pushing tags to Github will require adding an option to the `push` command
git push --tags
```

#### For questions/comment about repo maintenance contact
Asad Haris (aharis@uw.edu)

Dept. of Biostatistics

University of Washington



